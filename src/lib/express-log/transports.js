'use strict';
const fs = require('fs');
const path = require('path');
const moment = require('moment');
require('winston-daily-rotate-file');

/**
 * Customização dos transports do modulo winstonjs 
 * @param {object} winston Modulo winstonjs 
 * @param {object} app Modulo expressjs
 * @return {object} Retorna funções que representam transports customizados.
 */
module.exports = (winston, app, config) => {
    const { createLogger, transports, format } = winston;
    const { combine, timestamp, colorize, label, printf } = format;

    const getFileTransport = (config) => {
        return new transports.File({
            level: config.logLevel || 'debug',
            options: {
                flags: 'a+',
                encoding: config.enconding
            },
            maxsize: config.maxSize,
            maxFiles: config.maxFiles,
            tailable: false,
            filename: path.join(config.logFolder, (config.logName + '.log')),
            format: combine(
                label({ label: config.label }),
                timestamp(),
                printf(info => {
                    const data = {
                        source: (info.source || info.label),
                        level: info.level,
                        timestamp: moment().format(config.formatDate),
                        message: (info.request || info.message)
                    };
                    return JSON.stringify(data);
                })
            )
        });
    };

    const getFileRequestTransport = (config) => {
        return new (winston.transports.DailyRotateFile)({
            filename: `${path.join(config.logFolder, (config.logName))}-%DATE%.log`,
            datePattern: 'YYYY_MM_DD_HH_mm',
            zippedArchive: false,
            maxSize: config.maxSize,
            maxFiles: config.maxFiles
        });
    };

    const getConsoleTransport = (config) => {
        return new transports.Console({
            level: config.logLevel || 'debug',
            format: combine(
                colorize(),
                label({ label: 'server' }),
                timestamp(),
                printf(info => {
                    if (info.request) {
                        return `[${info.level}] ${moment().format(config.formatDate)} ${(info.source || info.label)} :\n${JSON.stringify(info.request, null, 4)}`;
                    } else {
                        return `[${info.level}] ${moment().format(config.formatDate)} ${(info.source || info.label)} - ${info.message}`;
                    }
                })
            )
        });
    };

    const serverfileTransport = getFileTransport(config.server);
    const serverConsoleTransport = getConsoleTransport(config.server);

    const requestFileTransport = getFileRequestTransport(config.requests);
    const requestConsoleTransport = getConsoleTransport(config.requests);

    ///// Cria loggers
    // logger de console (desenv)
    const serverLogger = createLogger({
        transports: [
            serverfileTransport,
            serverConsoleTransport
        ]
    });

    // logger de requests (principal)
    const requestLogger = createLogger({
        transports: [
            requestFileTransport,
            requestConsoleTransport
        ]
    });

    return {
        serverLogger,
        requestLogger
    };
};
module.exports = (app, config) => {
    const { serverLogger: logger } = app.get('logger');
    let response;
    try {
        const resolvers = require('./resolverFunctions')(app, config);
        response = resolvers;
    } catch (error) {
        logger.error('==================================================');
        logger.error('resolverFunctions error');
        logger.error(error);
        logger.error('==================================================');
    }

    return response;
};
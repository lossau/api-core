/**
 * @file Modulo de pesquisa de consumidores AMQP.
 * @since 2017-11-28
 */
const fs = require('fs');
const path = require('path');
const apps = [];
const _ = require('lodash');

const nomeModulo = 'scan-apps-amqp';


/**
 * Mapear script GraphQL
 * @param {object} app Modulo do Express
 * @return {array} 
 */
module.exports = (app, channel, config) => {
    const { serverLogger: logger } = app.get('logger');

    logger.log({
        level: 'info',
        source: nomeModulo,
        message: `Iniciado`
    });

    ///// objeto de lista de filas
    const consumers = [];

    ///// itera filas   
    logger.log({
        level: 'info',
        source: nomeModulo,
        message: `Finalizado - OK`
    });

    return new Promise((resolve, reject) => {

        ///// itera filas encontradas
        config.filas.map((fila) => {
            try {
                ///// inicia consumer
                const consumer = require('./consumer');

                // setup
                consumer.setup(app, channel, fila);
    
                //// add queue a objeto a lista de consumers
                consumers.push(fila.queueName);
    
            } catch (error) {
                logger.log({
                    level: 'error',
                    source: nomeModulo,
                    message: `Fila [${fila.queueName}] error: ${error}`
                });

                reject(error);
            }
        });
    
        return resolve(consumers);
    });
};
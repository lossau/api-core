'use strict';

module.exports = (config) => {
    return {
        headers: (config.mockHeaders ? getHeaders(config.mockHeaders) : null)
    };
}

/**
 * Transforma as chaves e valores de um objeto em uma string
 * @param {object} obj Objeto a ser transformado
 * Ex.: ?obj={ ip_origem: 177.70.116.104, data_registro_log: 2012-12-12T14:00:01 }
 * @return {string} String final
 * Ex: 'ip_origem': '177.70.116.104' 'data_registro_log': '2012-12-12T14:00:01'
 */
const getHeaders = (obj) => Object.keys(obj)
    .map((k) => `'${k}': '${obj[k]}', `)
    .reduce((acc,cur) => acc + cur, '')

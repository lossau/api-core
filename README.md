#Nodejs / Graphql Darwin

Motor de API Node.js com GraphQL e MongoDB.

## Arquivos de Configuração
amqp.json
database.json
redis.json
server.json

## Instruções de instalação

Usando o NPM:

```shell
$ npm install
```
## API Exemplo

No diretorio "./src/apps", contem pastas com o prefixo "graphql-". Nelas estão, os codigos de execução e o "GraphQL Query Language" (*.gql) de exemplo.  
Para adicionar uma API nova, é só criar uma nova pasta com o prefixo "graphql-" e utilizar o mesmo padrão de desenvolvimento das APIs de exemplo.

## Hora da diversão

Após iniciar o servidor com o comando (NODE_ENV != 'production'):

```shell
$ cd ./server/src
$ npm run start
```

Estará disponivel o endpoint ( http://localhost:4000/graphql ) do GraphQL no seu navegador. Onde sera possivel testar suas APIs em desenvolvimento.
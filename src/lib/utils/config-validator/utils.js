const URL = require('url').URL;


module.exports = {

    isValidUrl: (string) => {
        try {
            new URL(string);
            return true;
        } catch (error) {
            return false;
        }
    }

};
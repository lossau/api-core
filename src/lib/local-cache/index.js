'use strict';

const nomeModulo = 'local-cache';


module.exports = (app, config) => {

    const { serverLogger: logger } = app.get('logger');
    const ttl = config.ttl || 60;

    logger.log({
        level: 'info',
        source: nomeModulo,
        message: `Iniciado localCache com TTL: ${ttl}s`
    });

    const Cache = require('./cache')(logger);
    const cache = new Cache(ttl);

    return cache;

};

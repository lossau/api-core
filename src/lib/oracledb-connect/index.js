'use strict';

// Node do modulo
const nomeModulo = 'oracledb-connect';
// "oracledb": "^5.1.0",

/**
 * Obter conexão com o OracleDB
 * @param {function} app Servidor Express.
 * @param {function} config Json de configuração.
 * @return {Promise} Retorna uma promessa resolve. 
 */

module.exports = (app, configs) => {

    const { serverLogger: logger } = app.get('logger');

    return new Promise((resolve, reject) => {

        const oracledb = require('oracledb');
        oracledb.initOracleClient({ configDir: './src/lib/oracledb-connect' });

        logger.log({
            level: 'info',
            source: nomeModulo,
            message: `Iniciado`
        });

        // retorna CLOBs e NUMBERs como Strings
        oracledb.fetchAsString = [ oracledb.CLOB, oracledb.NUMBER];

        try {
            const conexoes = [];

            configs.forEach((config, index) => {

                const options = {
                    user: config.user,
                    password: config.password,
                    connectString: config.connectString,
                    poolMin: config.poolMin,
                    poolMax: config.poolMax
                };

                // conecta / cria pool
                oracledb.createPool(options).then(pool => {
                    
                    app.set('oracledb', oracledb);
                    
                    ///// loga ativo
                    logger.log({
                        level: 'info',
                        source: nomeModulo,
                        message: `OracleDB ativado com sucesso - ${config.alias}`
                    });

                    const database = {
                        name: config.alias,
                        database: pool
                    };
                    
                    // add a retorno
                    conexoes.push(database);

                    ///// loga ok
                    logger.log({
                        level: 'info',
                        source: nomeModulo,
                        message: `OracleDB conectado com sucesso ${config.alias}`
                    });

                    // ultima iteração
                    if(index == (configs.length -1)) {
                        resolve(conexoes);
                    }
                }).catch(err => {
                    logger.log({
                        level: 'error',
                        source: nomeModulo,
                        message: err
                    });
                    return reject(err);
                });

            });

        } catch (err) {
            logger.log({
                level: 'error',
                source: nomeModulo,
                message: err
            });
            return reject(err);
        }

    });
};
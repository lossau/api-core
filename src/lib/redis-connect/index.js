/**
 * @file Conexão com o Redis
 * @since 2017-12-28
 */
'use strict';
const bluebird = require('bluebird');
const Redis = bluebird.promisifyAll(require('ioredis'));
const redis = bluebird.promisifyAll(require('redis'));

// Node do modulo
const nomeModulo = 'redis-connect';

/**
 * Obter conexão com o Redis
 * @param {function} app Servidor Express.
 * @return {Promise} Retorna uma promise que resolve para o client do Redis 
 */
module.exports = (app, config) => {
    ///// cliente de acesso ao redis
    let client;
    let configType = (config.type === 'single'?'single': 'cluster');

    ///// logger
    const { serverLogger: logger } = app.get('logger');
    ///// seta url
    const url = config.type === 'single' ?
        config.url :
        `${JSON.stringify(config.nodes)}`;

    logger.log({
        level: 'info',
        source: nomeModulo,
        message: `Iniciado redis[${url}]`
    });

    if (config.type === 'single') {
        client = new redis.createClient(config.url);
    } else {
        client = new Redis.Cluster(config.nodes);
    }

    logger.log({
        level: 'info',
        source: nomeModulo,
        message: `config.type=${configType}`
    });

    let errorCount = 0;
    let maxErroCount = 5;
    client.on('error', (err) => {
        logger.log({
            level: 'error',
            source: nomeModulo,
            message: err
        });

        ///// incrementa quantidade de tentativas
        errorCount++;

        if(errorCount >= maxErroCount) {
            logger.log({
                level: 'error',
                source: nomeModulo,
                message: err
            });
            process.exit(1);
        }
    });


    client.on('connect', (result) => {        
        ///// seta cacheTest
        client.set('cacheTest', `cache iniciado ${new Date()} `);
        
        ///// busca cacheTest
        client.get('cacheTest', function(err, res) {
            logger.log({
                level: 'info',
                source: nomeModulo,
                message: `Finalizado - OK`
            });
            
            logger.log({
                level: 'info',
                source: nomeModulo,
                message: `cacheTest: - {${res}}`
            });
        });
    });

    return Promise.resolve(client);
};
const nomeModulo = "amqp-consumer";

const onMessage = (app, config) => (msg) => {
    const logger = app.get('logger').serverLogger;

    try {
        
        ///// identifica tipo de mensagem
        const method = msg.properties.type || config.methodName;
        ///// busca service
        const service = require(config.service)(app);
        ///// interpreta conteudo da mensagem
        const message = JSON.parse(msg.content);

        //// log
        logger.log({
            level: 'info',
            source: nomeModulo,
            message: `Mensagem recebida ${method} - ${JSON.stringify(message)}`
        });
    
        ///// executa mensagem recebida
        service[method](message)
            .then((resp) => {
                logger.log({
                    level: 'info',
                    source: nomeModulo,
                    message: `Mensagem processada ${method} - ${JSON.stringify(message)}`
                });
            }).catch(err => {
                logger.log({
                    level: 'error',
                    source: nomeModulo,
                    message: err
                });
            });
    } catch (error) {
        //// log
        logger.log({
            level: 'error',
            source: nomeModulo,
            message: error
        });
    }
};

const setup = (app, channel, config) => {
    const logger = app.get('logger').serverLog;
    const nomeModulo = `amqp-consumer-${config.queueName}`;



    channel.on('error', function (err) {
        logger.log({
            level: 'error',
            source: nomeModulo,
            message: err
        });
        process.exit(1);
    });

    try {
        //registra consumidor
        channel.consume (
    
            config.queueName,
    
            onMessage(app, config), {
                noAck: true
            }
        );
    } catch(err) {
        console.log('==================================================');
        console.log('setup err',err);
        console.log('==================================================');
    }
};

module.exports = {
    setup
};
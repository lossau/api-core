const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const { isValidUrl } = require('./utils');


/**
 * Função recursiva que valida propriedade de configuração, por tipo.
 * @param {object} chave Nome da propriedade
 * @param {object} valor Valor da propriedade
 * @param {object} regra Regra de validação da propriedade
 * @return {object} Retorna array de erros de validação
 */
const validarPropriedade = (chave, valor, regra) => {

    let erros = [];

    switch (regra.tipo) {

        case 'array':
            if (valor) {
                Object.keys(regra.props).map(p => {
                    valor.map(d => {
                        const errosArray = validarPropriedade(`${chave}.${p}`, d[p], regra.props[p]);
                        erros.push(...errosArray);
                    });
                });
            }
            else {
                erros.push(`${chave}: ${valor} - deve ser ${regra.tipo}`);
            }
            break;

        case 'ENUM':
            if (!regra.valores.includes(valor)) {
                erros.push(`${chave}: ${valor} - Deve conter um destes valores: ${regra.valores.join(', ')}`);
            }
            break;

        case 'dateFormat':
            if (!_.isString(valor)) {
                erros.push(`${chave}: ${valor} - deve ser ${regra.tipo}`);
            }
            break;

        case 'directory':
            if (!fs.existsSync(valor)) {
                erros.push(`${chave}: ${valor} - O diretório não existe`);
            }
            break;

        case 'file':
            if (!fs.existsSync(valor)) {
                erros.push(`${chave}: ${valor} - O arquivo não existe`);
            }
            break;

        case 'int':
            if (!_.isInteger(valor)) {
                erros.push(`${chave}: ${valor} - Deve ser ${regra.tipo}`);
            }
            break;

        case 'string':
            if (!_.isString(valor)) {
                erros.push(`${chave}: ${valor} - Deve ser ${regra.tipo}`);
            }
            break;

        case 'url':
            if (!isValidUrl(valor)) {
                erros.push(`${chave}: ${valor} - Deve ser ${regra.tipo}`);
            }
            break;
    }

    return erros;

};

/**
 * Busca schemas de configuração para validação das configurações do domínio
 * @param {object} appsDir Diretório dos apps do domínio
 * @param {object} config Configurações do domínio
 */
const validarConfiguracoes = (appsDir, config) => {

    return new Promise((resolve, reject) => {

        let configSchemas = [];
        let erros = [];

        // configSchema do domínio
        const rootSchemaPath = `${appsDir}/../configSchema.json`;

        configSchemas.push(rootSchemaPath);

        // configSchemas dos apps graphql e rest
        (fs.readdirSync(appsDir)).forEach((api) => {
            const dirAPI = path.join(appsDir, api);
            if (_.includes(api, 'graphql-') || _.includes(api, 'route-')) {
                const appSchemaPath = path.join(dirAPI, 'configSchema.json');
                configSchemas.push(appSchemaPath);
            }
        });

        // validação com os schemas existentes
        configSchemas.map(schemaPath => {

            if (fs.existsSync(schemaPath)) {

                let configSchema;

                // carrega o schema
                try {
                    configSchema = require(schemaPath);
                } catch (err) {
                    reject(err)
                }

                // valida o schema
                try {
                    Object.keys(configSchema).map(regra => {
                        const data = _.get(config, regra);
                        const errosProp = validarPropriedade(regra, data, configSchema[regra]);
                        if (errosProp) {
                            erros.push(...errosProp);
                        }
                    });
                } catch (err) {
                    reject(err);
                }

                if (erros.length > 0) {
                    // erros de validação
                    reject(erros.join(' | '));
                }
            }

        });

        resolve(true);

    });
};


module.exports = {
    validarConfiguracoes: (appsDir, config) => {
        return Promise.resolve(validarConfiguracoes(appsDir, config));
    }
};
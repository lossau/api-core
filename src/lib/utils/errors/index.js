class ValidationError extends Error {
    constructor(message, field) {
        super(message);
        this.field = field;
    }
}

function validate(errors) {
    console.log('=========================');
    console.log('errors',errors);
    console.log('=========================');
    if (errors) {
        throw new ValidationError(errors.message[0].message, errors.message[0].field);
    }
}

module.exports = validate;
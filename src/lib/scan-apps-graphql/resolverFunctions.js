const { ObjectID } = require('mongodb');
const { URL } = require('url');
const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const funcoes = [];
const queries = [];
const functions = [];
const mutations = [];
const nomeModulo = "resolverFunctions";

module.exports = (app, config) => {
    const { serverLogger: logger } = app.get('logger');

    /**
     * Função pra geração de mensagens de erro
     * @param {object} errorFormat mensagem de erro
     * @return {void} 
     */
    function logError(errorFormat) {
        logger.log({
            level: 'error',
            source: nomeModulo,
            message: `function.: ${errorFormat.functionResolved}`
        });
        logger.log({
            level: 'error',
            source: nomeModulo,
            message: `schema...: ${errorFormat.graphqlSchema}`
        });
    }

    const readResolvers = async(appsDir, logger) => {
        let tmp;
        ///// lista todos os apps
        (fs.readdirSync(appsDir)).forEach((api) => {
            // seta diretorio de app
            const dirAPI = path.join(appsDir, api);
            let resolverPath;
            // lista somente app que obedeça regex
            if (_.includes(api, 'graphql-')) {
                try {
                    tmp = api;
                    resolverPath = path.join(dirAPI, "resolver");
                    const resolver = require(resolverPath)(app);
                    funcoes.push(resolver);

                } catch (error) {
                    logger.info('==================================================');
                    logger.info('API:');
                    logger.info(tmp);
                    logger.info('resolver:');
                    logger.info(resolverPath);
                    logger.info('error:');
                    logger.info('==================================================');
                    const errorFormat = {
                        graphqlResolver: resolverPath
                    };
                    logger.log({
                        level: 'error',
                        source: nomeModulo,
                        message: error
                    });
                    logError(errorFormat);
                }
            }
        });
    }

    readResolvers(config.appsDir, logger);

    let tmp;
    try {
        funcoes.forEach((s) => {
            tmp = s;
            queries.push(s.queries);
            functions.push(s.functions);
            mutations.push(s.mutations);
        });
    } catch (error) {
        logger.info('==================================================');
        logger.info('funcoes tmp:');
        logger.info(tmp);
        logger.info('error:');
        logger.info(error);
        logger.info('==================================================');
    }


    let resolveToExport = {
        Query: {},
        Mutation: {}
    };

    try {
        let tmp;
        queries.forEach((q) => {
            tmp = {
                tipo: "query",
                query: q
            };
            Object.keys(q).forEach(function(key) {
                resolveToExport.Query[key] = q[key];
            });
        });

        mutations.forEach((q) => {
            tmp = {
                tipo: "mutation",
                mutation: q
            };
            Object.keys(q).forEach(function(key) {
                resolveToExport.Mutation[key] = q[key];
            });
        });

        functions.forEach((f) => {
            tmp = {
                tipo: "function",
                fn: f
            };
            Object.keys(f).forEach(function(key) {
                resolveToExport[key] = f[key];
            });

        });
    } catch (error) {
        logger.info('==================================================');
        logger.info('funcoes tmp:');
        logger.info(tmp);
        logger.info('error:');
        logger.info(error);
        logger.info('==================================================');
    }

    return resolveToExport;
};
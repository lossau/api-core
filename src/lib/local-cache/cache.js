const NodeCache = require('node-cache');

module.exports = (logger) => {

    class Cache {

        constructor(ttlSeconds) {
            this.cache = new NodeCache({
                stdTTL: ttlSeconds,
                checkperiod: ttlSeconds * 0.2,
                useClones: false
            });
        }

        get(key, storeFunction) {
            const value = this.cache.get(key);
            if (value) {
                logger.info('localCache HIT')
                return Promise.resolve(value);
            }

            logger.info('localCache MISS');

            return storeFunction().then((result) => {
                logger.info(`localCache SET: ${key}`);
                this.cache.set(key, result);
                return result;
            });
        }

        del(keys, deleteFunction) {
            logger.info(`localCache DEL: ${keys}`);
            this.cache.del(keys);
        }

        delStartWith(startStr = '') {
            if (!startStr) {
                return;
            }

            const keys = this.cache.keys();
            for (const key of keys) {
                if (key.indexOf(startStr) === 0) {
                    this.del(key);
                }
            }
        }

        flush() {
            this.cache.flushAll();
        }
    }

    return Cache;

}
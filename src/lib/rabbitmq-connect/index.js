'use strict';

const amqp = require('amqplib');

// Node do modulo
const nomeModulo = 'rabbitmq-connect';

/**
 * Obter conexão com o RabbitMQ
 * @param {function} app Servidor Express.
 * @return {Promise} Retorna uma promise que resolve o channel do Rabbit 
 */
module.exports = (app, config) => {
    const { serverLogger: logger } = app.get('logger');

    logger.log({
        level: 'info',
        source: nomeModulo,
        message: `Iniciado brokerURI[${config.brokerURI}]`
    });

    return (amqp
        .connect(config.brokerURI)
        .then(conn => {
            app.set('conexaoRabbitMQ', conn);
            return conn.createChannel();
        }));
};
const fs = require('fs');
const path = require('path');
// Diretorio das APIs em GraphQL
const schemas = [];
const apps = [];
const { makeExecutableSchema } = require('graphql-tools');
const _ = require('lodash');
const nomeModulo = 'definition';

/**
 * Mapear script GraphQL
 * @param {object} app Modulo do Express
 * @return {array} 
 */
module.exports = (app, config) => {
    const { serverLogger: logger } = app.get('logger');
    const resolvers = require('./resolvers')(app, config);

    async function readSchema(dirApps) {

        (fs.readdirSync(dirApps)).forEach((app) => {
            const dirAPI = path.join(dirApps, app);

            // lista somente app que obedeça regex
            if (_.includes(app, 'graphql-')) {
                try {

                    const schemaPath = path.join(dirAPI, 'schema');
                    const schema = require(schemaPath);
                    schemas.push(schema);
                    apps.push(app.replace('graphql-', ''));
                } catch (error) {
                    logger.info('==================================================');
                    logger.info('readSchema error');
                    logger.info(error);
                    logger.info('==================================================');
                    const errorFormat = {
                        graphqlSchema: schemaPath
                    };
                    logger.log({
                        level: 'error',
                        source: nomeModulo,
                        message: error
                    });
                    logError(errorFormat);
                }
            }
        });
    }

    //// le esquemas
    readSchema(config.appsDir);

    if (schemas.length > 0) {
        const types = [];
        const queries = [];
        const mutations = [];

        ///// itera propriedades de app graphql
        schemas.forEach((s) => {
            types.push(s.types);
            queries.push(s.queries);
            mutations.push(s.mutations);
        });

        // Define your types here.
        const typeDefs = `
        ${types.join('\n')}

        type Query {
          ${queries.join('\n')}
        }

        type Mutation {
            ${mutations.join('\n')}
        }
        `;

        // Generate the schema object from your types definition.type
        return {
            schema: makeExecutableSchema({ typeDefs, resolvers }),
            apps: apps
        };
    }
    else {
        return null;
    }

};
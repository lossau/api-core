const express = require('express');
const async = require('async');
const app = express();
const utils = require('./lib/utils');

const { validarConfiguracoes } = utils.configValidator;

const start = (config, middlewares) => {

    // utilitários
    app.set('utils', utils);

    async.auto({

        configValidator: (callback) => {

            validarConfiguracoes(config.appsDir, config)
                .then(res => {
                    // configurações do app
                    app.set('config', config);
                    callback(null, res);
                })
                .catch(err => {
                    callback(err)
                });
        },

        logger: (callback) => {
            // Configurando de log no Express
            const logger = require('./lib/express-log')(app, config);
            callback(null, logger);
        },

        modules: ['logger', (_, callback) => {
            // Incluindo middlewares do express.js
            require('./lib/express-modules')(app);
            callback();
        }],

        mongodb: ['logger', (_, callback) => {
            // Monta conexão do MongoDB
            if (config.mongodb === undefined) {
                callback(null);
            } else {

                const conexoes = require('./lib/mongodb-connect')(app, config.mongodb);

                // resolve
                conexoes.then(conexoes => {
                    conexoes.forEach((conexao, index) => {
                        ///// add ao database
                        app.set(conexao.name, conexao.database);

                        if (index == (conexoes.length - 1)) {
                            callback(null, conexoes);
                        }
                    });


                }).catch(error => {
                    callback(error);
                });

            }
        }],

        oracledb: ['logger', (_, callback) => {
            // Monta conexão do OracleDB
            if (config.oracleDb === undefined) {
                callback(null)
            } else {
                require('./lib/oracledb-connect')(app, config.oracleDb)
                    .then(conexoes => {
                        conexoes.forEach((conexao, index) => {
                            ///// add ao database
                            app.set(conexao.name, conexao.database);

                            if (index == (conexoes.length - 1)) {
                                callback(null, conexoes);
                            }
                        });
                    })
                    .catch(err => callback(err));
            }
        }],

        soap: ['logger', (_, callback) => {
            if (config.soap === undefined) {
                callback(null)
            } else {
                require('./lib/soap-connect')(app, config.soap)
                    .then(conexoes => {
                        conexoes.forEach((conexao, index) => {
                            ///// add ao database
                            app.set(conexao.name, conexao.webservice);

                            if (index == (conexoes.length - 1)) {
                                callback(null, conexoes);
                            }
                        });
                    }).catch(err => callback(err));
            }
        }],

        rabbitmq: ['logger', 'redis', (_, callback) => {
            // Monta conexão do RabbitMQ
            if (config.queues) {
                try {
                    require('./lib/rabbitmq-connect')(app, config.queues).then(resp => {
                        app.set('rabbitmq', resp);
                        callback(null, resp);
                    });
                } catch (err) {
                    console.log('==================================================');
                    console.log('err', err);
                    console.log('==================================================');
                    callback(err)
                }
                //callback(null, connect);
            } else {
                callback(null, null);
            }
        }],

        redis: ['logger', (_, callback) => {
            // Monta conexão do Redis
            if (config.redis === undefined) {
                callback(null)
            } else {
                require('./lib/redis-connect')(app, config.redis)
                    .then(client => {
                        app.set('clienteRedis', client);
                        callback(null, client)
                    })
                    .catch(err => callback(err));
            }
        }],

        localCache: ['logger', (_, callback) => {
            if (config.localCache === undefined) {
                callback(null)
            } else {
                const localCache = require('./lib/local-cache')(app, config.localCache);
                app.set('localCache', localCache);
                callback(null, localCache);
            }
        }],

        amqp: ['mongodb', 'oracledb', 'rabbitmq', 'redis', 'soap', (_, callback) => {
            // Obtem todos os consumers de mensagens - RabbitMQ
            let amqp;

            if (config.queues) {
                amqp = require('./lib/scan-apps-amqp')(app, _.rabbitmq, config.queues)
                    .then(amqp => callback(null, amqp))
                    .catch(err => callback(err));
            } else {
                callback(null, amqp);
            }
        }],

        graphql: ['mongodb', 'redis', 'oracledb', 'soap', 'localCache', (_, callback) => {
            // Obtem todas as APIs GraphQL
            const graphql = require('./lib/scan-apps-graphql')(app, _.mongodb, _.oracledb, _.redis, _.rabbitmq, config, middlewares);
            callback(null, graphql);
        }],

        rest: ['mongodb', 'redis', 'soap', 'localCache', (_, callback) => {
            // Obtem todas as APIs REST
            require('./lib/scan-apps-rest')(app, config).then(routes => callback(null, routes));
        }]

    }, (err, {
        logger: { serverLog: logger },
        rest,
        graphql,
        amqp
    }) => {

        ///// caso não haja erro
        if (!err) {
            ///// Inicia servidor
            const port = process.env.PORT || config.port;

            // logger.info('==================================================');
            // logger.info('Iniciando dominio com as configurações:');
            // logger.info(JSON.stringify(config, null, 4));
            // logger.info('==================================================');

            app.listen(port, () => {
                logger.info(`Executando ${config.serverName} na URL: http://${config.hostName}:${config.port} (${(process.env.NODE_ENV || 'dev')})`);
                ///// loga graphql
                graphql.forEach(service => logger.info(`GraphQL registrado.: ${service}`));
                if (amqp) {
                    amqp.forEach(consumer => logger.info(`Consumer amqp registrado.: ${consumer}`));
                }
                rest.forEach(route => logger.info(`REST registrado....: ${route.uri} [${route.method}]`));

                // processo pode ser considerado READY
                process.send ? process.send('ready') : () => {};

            });

            process.on('uncaughtException', (err) => {
                logger.log({
                    level: 'error',
                    source: 'server',
                    message: err
                });
                process.exit(1);
            });
        } else {
            logger.log({
                level: 'error',
                source: 'server',
                message: err
            });
            process.exit(1);
        }

        const gracefullyStop = async () => {

            try {
                const clienteMongo = app.get('clienteMongo')
                if (clienteMongo) {
                    await clienteMongo.close();
                    logger.info('MongoDB - Conexão encerrada');
                }
            } catch (err) {
                logger.warn(`Erro ao encerrar conexão MongoDB: ${err}`);
                process.exit(1);
            }

            try {
                const conexaoOracleDb = app.get('oracledb')
                if (conexaoOracleDb) {
                    await conexaoOracleDb.getPool().close(10)
                    logger.info('OracleDB - Conexão encerrada');
                }
            } catch (err) {
                logger.warn(`Erro ao encerrar conexão OracleDB: ${err}`);
                process.exit(1);
            }

            try {
                const clienteRedis = app.get('clienteRedis')
                if (clienteRedis) {
                    clienteRedis.quit();
                    logger.info('Redis - Conexão encerrada');
                }
            } catch (err) {
                logger.warn(`Erro ao encerrar conexão Redis: ${err}`);
                process.exit(1);
            }

            try {
                const conexaoRabbitMQ = app.get('conexaoRabbitMQ');
                const canalRabbitMQ = app.get('rabbitmq');
                if (conexaoRabbitMQ) {
                    await canalRabbitMQ.close()
                    await conexaoRabbitMQ.connection.close()
                    logger.info('RabbitMQ - Conexão encerrada');
                }
            } catch (err) {
                logger.warn(`Erro ao encerrar conexão RabbitMQ: ${err}`);
                process.exit(1);
            }

            process.exit(0);

        }

        process.on('SIGINT', async function() {
            gracefullyStop();
        });

        process.on('SIGUSR1', async function() {
            gracefullyStop();
        });

        process.on('SIGUSR2', async function() {
            gracefullyStop();
        });

        process.on('SIGTERM', async function() {
            gracefullyStop();
        });

        process.on('uncaughtException', async function() {
            gracefullyStop();
        });

    });
};

module.exports = {
    start,
    app,
    utils,
    validarConfiguracoes
};

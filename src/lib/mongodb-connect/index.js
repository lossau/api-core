'use strict';
const { MongoClient } = require('mongodb');

// Node do modulo
const nomeModulo = 'mongodb-connect';

/**
 * Obter conexão com o MongoDB
 * @param {function} app Servidor Express.
 * @param {function} config Json de configuração.
 * @return {Promise} Retorna uma promessa resolve.
 */
module.exports = (app, configs) => {


    return new Promise((resolve, reject) => {
        const { serverLogger: logger } = app.get('logger');

        const conexoes = [];

        configs.forEach((config, index) => {

            let uri = '';

            if (config.connectionString) {
                uri = config.connectionString;
            } else {
                uri = `mongodb://${config.user}:${encodeURIComponent(config.password)}@${config.host}:${config.port}/${config.database}`;
            }

            const options = {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            };

            logger.log({
                level: 'info',
                source: nomeModulo,
                message: `Iniciado`
            });

            // conecta
            MongoClient.connect(uri, options).then(client => {

                app.set('clienteMongo', client)

                ///// loga ativo
                logger.log({
                    level: 'info',
                    source: nomeModulo,
                    message: `MongoDB ativado com sucesso - ${config.alias}`
                });

                const database = {
                    name: config.alias,
                    database: client.db(config.database)
                };

                ///// loga ok
                logger.log({
                    level: 'info',
                    source: nomeModulo,
                    message: `MongoDB conectado com sucesso ${config.alias}`
                });

                conexoes.push(database);

                // caso seja o ultimo
                if (index == (configs.length - 1)) {
                    resolve(conexoes);
                }
            })

        });
    });

};

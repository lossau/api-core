'use strict';
const winston = require('winston');
const zlib = require('zlib');
const { createLogger } = winston;

/**
 * Função que disponibiliza o modulo de log pra cadastro no express.js
 * @param {object} app express().
 * @param {object} config fileConfig.
 * @return {function} Retorna o modulo "logger" do winston.
 */
module.exports = (app, config) => {
    // transports customizados
    const {
        serverLogger,
        requestLogger,
    } = require('./transports')(winston, app, config.log);

    /**
     * Função de geração de log no express.
     * @param {object} req Request (express) 
     * @param {object} res Response (express)
     * @param {function} next Next (express)
     */

    function expressLogger(req, res, next) {

        let oldWrite = res.write;
        let oldEnd = res.end;
        let chunks = [];

        res.write = function (chunk) {

            chunks.push(chunk);

            oldWrite.apply(res, arguments);

            let body = null;
            try {
                const _compact = Buffer.concat(chunks);
                const _buffer = zlib.gunzipSync(_compact);
                const _text = _buffer.toString('utf8');
                body = JSON.parse(_text);
            } catch (e1) {
                try {
                    body = JSON.parse(chunks[0]);
                } catch (e2) {
                    body = {};
                }
            }

            // ignora requests internas (GET e IntrospectionQuery)
            if (req.method === 'POST' && req.body.query && !req.body.query.includes('IntrospectionQuery')) {
                const { ip_origem, data_registro_log, id_requisicao, data_sistema_origem, id_sistema_origem, metodo } = req.headers;
                const tipo_aplicacao = config.applicationType ? config.applicationType : '-'; // graphqldominio
                const nome_aplicacao = config.serverName ? config.serverName : '-'; // Template
                const nome_classe = 'logger'; // nomeArquivoJS
                const nome_operacao = metodo ? metodo : '-'; // nomeQuery
                const nivel_log = 'INFO'; // ERROR
                const mensagem_log = '-'; // erro resolver 1
                const indicador_error = '-'; // error
                const codigo_erro = '-'; // 404001
                const mensagem_erro = '-'; // NullPointerException
                const elapsed_time_ms = '-'; // 100

                const request = {
                    headers: req.headers,
                    params: req.params,
                    query: req.query,
                    body: req.body
                };

                const response = {
                    headers: res._headers,
                    body: body
                };

                const log = `[${ip_origem}][${data_registro_log}][${id_requisicao}][${data_sistema_origem}][${id_sistema_origem}][${tipo_aplicacao}][${nome_aplicacao}][${nome_classe}][${nome_operacao}][${nivel_log}][${mensagem_log}][${indicador_error}][${codigo_erro}][${mensagem_erro}][${elapsed_time_ms}][${JSON.stringify(request)}][${JSON.stringify(response)}]`;
                const logLevel = config.log.requests.logLevel || 'debug';

                try {

                    const error = res.statusCode >= 400;

                    if (error) {
                      requestLogger.error(log);

                    } else {
                        requestLogger.info(log);
                    }

                } catch (_) {
                    console.log(log);
                }
            }

        };

        res.end = function (chunk) {
            if (chunk)
                chunks.push(chunk);
            oldEnd.apply(res, arguments);
        };

        next();
    }

    // Armazenando logger no servidor
    app.set('logger', {
        serverLogger,
        requestLogger
    });

    // Incluindo gerador de log pelo Express
    app.use(expressLogger);

    return {
        serverLog: serverLogger,
        requestLog: requestLogger
    };
};
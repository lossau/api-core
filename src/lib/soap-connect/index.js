'use strict';

// Node do modulo
const nomeModulo = 'soap-connect';

/**
 * Obter conexão com o Soap
 * @param {function} app Servidor Express.
 * @param {function} config Json de configuração.
 * @return {Promise} Retorna uma promessa resolve. 
 */

module.exports = (app, configs) => {

    const { serverLogger: logger } = app.get('logger');

    return new Promise((resolve, reject) => {

        const soap = require('soap');

        logger.log({
            level: 'info',
            source: nomeModulo,
            message: `Iniciado`
        });

        try {
            const conexoes = [];

            configs.map(async (config, index) => {

                try {

                    ///// loga ativo
                    logger.log({
                        level: 'info',
                        source: nomeModulo,
                        message: `SOAP ativado com sucesso - ${config.alias}`
                    });

                    const client = await soap.createClientAsync(config.wsdsl);

                    // add a retorno
                    conexoes.push({
                        name: config.alias,
                        webservice: client
                    });

                    ///// loga ok
                    logger.log({
                        level: 'info',
                        source: nomeModulo,
                        message: `SOAP conectado com sucesso ${config.alias}`
                    });

                    // ultima iteração
                    if (index == (configs.length - 1)) {
                        resolve(conexoes);
                    }

                } catch (err) {
                    logger.log({
                        level: 'error',
                        source: nomeModulo,
                        message: err
                    });
                    return reject(err);
                }

            });

        } catch (err) {
            logger.log({
                level: 'error',
                source: nomeModulo,
                message: err
            });
            return reject(err);
        }
    });
};
'use strict';
const {
    makeExecutableSchema
} = require('graphql-tools');
const {
    graphqlExpress,
    graphiqlExpress
} = require('apollo-server-express');
const bodyParser = require('body-parser');
const Context = require('../context-app');
// Node do modulo
const nomeModulo = 'scan-apps-graphql';


/**
 * Mapear script GraphQL
 * @param {object} app Modulo do Express
 * @param {object} mongodb Modulo MongoDB
 * @param {object} oracledb Modulo OracleDB
 * @param {object} config Json de Configuração
 * @return {array} 
 */
module.exports = (app, mongodb, oracledb, redis, rabbitmq, config, middlewares) => {
    const { serverLogger: logger } = app.get('logger');
    const { headers: mockHeaders } = require('../utils/config')(config);

    // fix redis
    app.set('redis', redis);

    /**
     * Função pra geração de mensagens de erro
     * @param {object} errorFormat mensagem de erro
     * @return {void} 
     */
    function logError(errorFormat) {
        logger.log({
            level: 'error',
            source: nomeModulo,
            message: `function.: ${errorFormat.functionResolved}`
        });
        logger.log({
            level: 'error',
            source: nomeModulo,
            message: `schema...: ${errorFormat.graphqlSchema}`
        });
    }

    function scanGraphql() {

        try {
            const definition = require('./definition')(app, config);
            if (definition) {
                const buildOptions = async (req, res) => {
                    //const user = await authenticate(req, mongo.Users);
                    return {
                        // This context object is passed to all resolvers.
                        context: {
                            mongo: mongodb,
                            oracledb: oracledb,
                            cache: redis,
                            amqp: rabbitmq,
                            config: config,
                            logger: logger
                        },
                        schema: definition.schema
                    };
                };

                if (middlewares) {
                    logger.log({
                        level: 'info',
                        source: nomeModulo,
                        message: 'Adicionado middlewares opcionais'
                    });
                    app.use('/graphql', bodyParser.json(), middlewares, graphqlExpress(buildOptions));
                } else {
                    app.use('/graphql', bodyParser.json(), graphqlExpress(buildOptions));
                }

                if (process.env.NODE_ENV !== 'production') {
                    app.use('/graphiql', graphiqlExpress({
                        // endpoint de playground graphql
                        endpointURL: '/graphql',
                        // header de mock
                        passHeader: mockHeaders
                    }));
                }

                logger.log({
                    level: 'info',
                    source: nomeModulo,
                    message: 'GraphqlHTTP ativado com sucesso!'
                });

                return definition.apps;
            }
            else {
                return [];
            }

        } catch (error) {
            logger.log({
                level: 'error',
                source: nomeModulo,
                message: error
            });
            logger.log({
                level: 'error',
                source: nomeModulo,
                message: 'GraphqlHTTP não pode ser ativo!'
            });
            return [];
        }
    }

    return scanGraphql();
};